import sys
import threading
from urllib.request import urlopen, Request


class GenerateTraffic:
    i = 0

    def fetch_data_from_server(self, url):
        try:
            Request(url)
            response = urlopen(url)
            response.read()
        except Exception as e:
            print(e)
        finally:
            self.i = self.i - 1

    def start(self, url, numberOfWorkers):
        self.i = 0
        while True:
            if self.i < numberOfWorkers:
                self.i = self.i + 1
                try:
                    threading._start_new_thread(self.fetch_data_from_server, (url,))
                    print("Amount of worker: " + str(self.i))
                except Exception as e:
                    print(e)


url = sys.argv[1]
numberOfWorkers = int(sys.argv[2])
print("Starting pulling data from: " + url + " in " + str(numberOfWorkers) + "threads")
traffic = GenerateTraffic()
traffic.start(url, numberOfWorkers)
